package enums

var enumStringMap = map[string]string{

	// Account Roles
	"ROLE_OWNER":  "owner",
	"ROLE_WRITER": "writer",

	// Account Provider Type
	"PROVIDER_TYPE_DEFAULT": "default",
	"PROVIDER_TYPE_SHOPIFY": "shopify",

	// Account Billing Providers
	"PROVIDER_STRIPE":  "stripe",
	"PROVIDER_SHOPIFY": "shopify",

	// Transaction Types
	"TRANSACTION_TYPE_DEPOSIT": "deposit",
	"TRANSACTION_TYPE_CHARGE":  "charge",
	"TRANSACTION_TYPE_REFUND":  "refund",

	// Transaction Status
	"TRANSACTION_STATUS_PENDING": "pending",
	"TRANSACTION_STATUS_SUCCESS": "success",
	"TRANSACTION_STATUS_EXPIRED": "expired",
	"TRANSACTION_STATUS_FAILED":  "failed",

	// Plans
	"PLAN_STARTER":        "starter",
	"PLAN_GROWING":        "growing",
	"PLAN_STARTER_YEARLY": "starter_yearly",
	"PLAN_GROWING_YEARLY": "growing_yearly",

	// Invite Status
	"STATUS_PENDING":   "pending",
	"STATUS_ACCEPTED":  "accepted",
	"STATUS_CANCELLED": "cancelled",
	"STATUS_EXPIRED":   "expired",

	// Refund Request Status
	"REQUEST_STATUS_PENDING":   "pending",
	"REQUEST_STATUS_ACCEPTED":  "accepted",
	"REQUEST_STATUS_CANCELLED": "cancelled",
	"REQUEST_STATUS_EXPIRED":   "expired",
	"REQUEST_STATUS_REJECTED":  "rejected",

	// Refund Request Issue
	"TYPE_GRAMMAR_ISSUES":   "grammar_issues",
	"TYPE_WRONG_PRODUCT":    "wrong_product",
	"TYPE_INCOMPLETE_WORDS": "incomplete_words",
	"TYPE_OTHER_ISSUE":      "other_issue",

	// Content Generation Models
	"MODEL50_PD":    "model50_pd",
	"MODEL50_PT":    "model50_pt",
	"MODEL50_MD":    "model50_md",
	"MODEL50_AT":    "model50_at",
	"MODEL50_FB":    "model50_fb",
	"MODEL50_FB_AD": "model50_fb_ad",
	"MODEL50_IG":    "model50_ig",
	"MODEL50_IG_AD": "model50_ig_ad",
	"MODEL50_TW":    "model50_tw",
	"MODEL50_TW_AD": "model50_tw_ad",
	"MODEL50_TK":    "model50_tk",
	"MODEL50_SC":    "model50_sc",
	"MODEL50_AMZN":  "model50_amzn",
	"MODEL50_ESL":   "model50_esl",
	"MODEL50_BPD":   "model50_bpd",

	// Request Types
	"RESOURCE_TYPE_CONTENT_GENERATION": "content_generation",
	"RESOURCE_TYPE_FEATURE_STORE":      "feature_store",

	// Collection Item Entry Mode
	"ENTRY_MODE_MANUAL":    "manual",
	"ENTRY_MODE_AUTOMATIC": "automatic",
}

var stringFullMap = map[string]string{
	// Providers
	"stripe":  "Stripe",
	"shopify": "Shopify",

	// Transaction Types
	"deposit": "Deposit",
	"charge":  "Charge",
	"refund":  "Refund",

	// Account Roles
	"owner":  "Owner",
	"writer": "Writer",

	// Transaction Status
	"pending": "Pending",
	"success": "Success",
	"expired": "Expired",
	"failed":  "Failed",

	// Plans
	"starter":        "Starter Monthly",
	"growing":        "Growing Monthly",
	"starter_yearly": "Starter Yearly",
	"growing_yearly": "Growing Yearly",

	// Account Source
	"default": "Default",

	// Status (In general)
	"accepted":  "Accepted",
	"cancelled": "Cancelled",
	"rejected":  "Rejected",

	// Refund Request IssueType
	"wrong_product":    "Wrong Product",
	"grammar_issues":   "Grammar Issue",
	"incomplete_words": "Incomplete Words",
	"other_issue":      "Other",

	// Resource Type
	"content_generation": "Content Generation",
	"feature_store":      "Feature Store",
}

type EnumType string

const (
	TransactionTypeEnum            EnumType = "TransactionType"
	TransactionStatusEnum          EnumType = "TransactionStatus"
	ResourceTypeEnum               EnumType = "ResourceType"
	PaymentPlanEnum                EnumType = "PaymentPlan"
	InviteStatusEnum               EnumType = "InviteStatus"
	BillingProviderEnum            EnumType = "BillingProvider"
	AccountProviderType            EnumType = "AccountProviderType"
	OrganizationExternalSourceEnum EnumType = "OrganizationExternalSource"
	ContentGenerationModelEnum     EnumType = "ContentGenerationModel"
	RefundRequestIssueTypeEnum     EnumType = "RefundRequestIssueType"
	RefundRequestStatusEnum        EnumType = "RefundRequestStatus"
	OrganizationRoleEnum           EnumType = "OrganizationRole"
	CollectionItemEntryModeEnum    EnumType = "OrganizationCollectionItem_EntryMode"
)

var stringToEnumMap = map[EnumType]map[string]string{
	TransactionTypeEnum: {
		"deposit": "TRANSACTION_TYPE_DEPOSIT",
		"charge":  "TRANSACTION_TYPE_CHARGE",
		"refund":  "TRANSACTION_TYPE_REFUND",
	},

	TransactionStatusEnum: {
		"pending": "TRANSACTION_STATUS_PENDING",
		"success": "TRANSACTION_STATUS_SUCCESS",
		"expired": "TRANSACTION_STATUS_EXPIRED",
		"failed":  "TRANSACTION_STATUS_FAILED",
	},

	PaymentPlanEnum: {
		"starter":        "PLAN_STARTER",
		"growing":        "PLAN_GROWING",
		"starter_yearly": "PLAN_STARTER_YEARLY",
		"growing_yearly": "PLAN_GROWING_YEARLY",
	},

	InviteStatusEnum: {
		"pending":   "STATUS_PENDING",
		"accepted":  "STATUS_ACCEPTED",
		"cancelled": "STATUS_CANCELLED",
		"expired":   "STATUS_EXPIRED",
	},

	BillingProviderEnum: {
		"stripe":  "PROVIDER_STRIPE",
		"shopify": "PROVIDER_SHOPIFY",
	},

	AccountProviderType: {
		"default": "PROVIDER_TYPE_DEFAULT",
		"shopify": "PROVIDER_TYPE_SHOPIFY",
	},

	OrganizationExternalSourceEnum: {
		"default": "SOURCE_DEFAULT",
		"shopify": "SOURCE_SHOPIFY",
	},

	OrganizationRoleEnum: {
		"owner":  "ROLE_OWNER",
		"writer": "ROLE_WRITER",
	},

	RefundRequestStatusEnum: {
		"pending":   "REQUEST_STATUS_PENDING",
		"accepted":  "REQUEST_STATUS_ACCEPTED",
		"cancelled": "REQUEST_STATUS_CANCELLED",
		"rejected":  "REQUEST_STATUS_REJECTED",
	},

	RefundRequestIssueTypeEnum: {
		"wrong_product":    "TYPE_WRONG_PRODUCT",
		"grammar_issues":   "TYPE_GRAMMAR_ISSUES",
		"incomplete_words": "TYPE_INCOMPLETE_WORDS",
		"other_issue":      "TYPE_OTHER_ISSUE",
	},

	ContentGenerationModelEnum: {
		"model50_pd":    "MODEL50_PD",
		"model50_pt":    "MODEL50_PT",
		"model50_md":    "MODEL50_MD",
		"model50_at":    "MODEL50_AT",
		"model50_fb":    "MODEL50_FB",
		"model50_fb_ad": "MODEL50_FB_AD",
		"model50_ig":    "MODEL50_IG",
		"model50_ig_ad": "MODEL50_IG_AD",
		"model50_tw":    "MODEL50_TW",
		"model50_tw_ad": "MODEL50_TW_AD",
		"model50_tk":    "MODEL50_TK",
		"model50_sc":    "MODEL50_SC",
		"model50_amzn":  "MODEL50_AMZN",
		"model50_esl":   "MODEL50_ESL",
		"model50_bpd":   "MODEL50_BPD",
	},

	ResourceTypeEnum: {
		"content_generation": "RESOURCE_TYPE_CONTENT_GENERATION",
		"feature_store":      "RESOURCE_TYPE_FEATURE_STORE",
	},

	CollectionItemEntryModeEnum: {
		"manual":    "ENTRY_MODE_MANUAL",
		"automatic": "ENTRY_MODE_AUTOMATIC",
	},
}

type EnumValue interface {
	String() string
}

func ToString(enum EnumValue) string {
	if val, ok := enumStringMap[enum.String()]; ok {
		return val
	}
	return enum.String()
}

func ToFullString(enum EnumValue) string {
	return stringFullMap[ToString(enum)]
}

func ToEnum(_type EnumType, input string) string {
	if val, ok := stringToEnumMap[_type]; ok {
		if enum, ok := val[input]; ok {
			return enum
		}
	}
	return input
}
